/*
Nome: Kaio Cesar Spina
Aula: 09
Prof: Luciano Rossi
Herença e Classes Abstratas
 */
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class UsaProduto {

    public static void main(String[] args) {

        Eletronico e = new Eletronico();
        e.cadastraProduto();
        e.print();
        
        JOptionPane.showMessageDialog(null, "Iremos cadastrar um item de vestuário qualquer.");
        Vestuario f = new Vestuario();
        f.cadastraProduto();
        f.print();
        
        JOptionPane.showMessageDialog(null, "Iremos cadastrar uma peça de roupa.");
        Roupa k = new Roupa();
        k.cadastraProduto();
        k.print();
        
        JOptionPane.showMessageDialog(null, "Iremos cadastrar um calçado.");
        Calcado j = new Calcado();
        j.cadastraProduto();
        j.print();
        }
    
}
