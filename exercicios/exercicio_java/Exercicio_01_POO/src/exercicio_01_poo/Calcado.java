
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class Calcado extends Vestuario {
    String materialSola, materialParteSuperior, materialInterno;
    Calcado(){}
    Calcado(int c, String d, float v, String n, String cr, String tm, String ms, String mps, String mi){
        super(c, d, v, n, cr, tm);
        this.materialSola = ms;
        this.materialParteSuperior = mps;
        this.materialInterno = mi;
    }

    public String getMaterialSola() {
        return materialSola;
    }

    public void setMaterialSola(String materialSola) {
        this.materialSola = materialSola;
    }

    public String getMaterialParteSuperior() {
        return materialParteSuperior;
    }

    public void setMaterialParteSuperior(String materialParteSuperior) {
        this.materialParteSuperior = materialParteSuperior;
    }

    public String getMaterialInterno() {
        return materialInterno;
    }

    public void setMaterialInterno(String materialInterno) {
        this.materialInterno = materialInterno;
    }
    
    public void print(){
                JOptionPane.showMessageDialog(null,
                        "Código do produto: "+this.getCodigo()+
                                "\nDescrição do produto: "+this.getDescricao()+
                                "\nValor do produto R$ "+this.getValor()+
                                "\nMarca: "+this.getNome()+
                                "\nCor: "+this.getCor()+
                                "\nTamanho: "+this.getTamanho()+
                                "\nMaterial sola: "+this.getMaterialSola()+
                                "\nMaterial parte superior: "+this.getMaterialParteSuperior()+
                                "\nMaterial interno: "+this.getMaterialInterno());
    }
      @Override
    
    public void cadastraProduto() {
        super.cadastraProduto();
        setMaterialSola(JOptionPane.showInputDialog("Informe o tipo do material da sola:"));
        setMaterialParteSuperior(JOptionPane.showInputDialog("Informe o tipo do material superior: "));
        setMaterialInterno(JOptionPane.showInputDialog("Informe o tipo do material interno:"));
    }
}