
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class Eletronico extends Produto {
    private int voltagem;
    Eletronico(){}
    Eletronico (int c, String d, float v, int vo){
        super (c, d, v);
        this.voltagem = vo;
    }
    
    public int getVoltagem() {
        return voltagem;
    }

    public void setVoltagem(int voltagem) {
        this.voltagem = voltagem;
    }
    
    public void print(){
                JOptionPane.showMessageDialog(null,
                "Código do produto: "+this.getCodigo()+
                "\nDescrição do produto: "+this.getDescricao()+
                "\nValor do produto R$ "+this.getValor()+
                "\nVoltagem eletrônico "+this.getVoltagem()+"v");
    }
    
    @Override
    public void cadastraProduto() {
        JOptionPane.showMessageDialog(null, "Iremos cadastrar um produto eletrônico.");
        super.cadastraProduto();
        setVoltagem(Integer.parseInt(JOptionPane.showInputDialog("Informe a voltagem: ")));
    }    
    }


