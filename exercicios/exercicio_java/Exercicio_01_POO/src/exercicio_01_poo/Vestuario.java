
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class Vestuario extends Produto {
    private String nome;
    private String cor;
    private String tamanho;
    Vestuario(){}
    Vestuario (int c, String d, float v, String n, String cr, String tm){
        super( c, d , v);
        this.cor = cr;
        this.nome = n;
        this.tamanho = tm;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }
    
    public void print(){
                JOptionPane.showMessageDialog(null,
                "Código do produto: "+this.getCodigo()+
                "\nDescrição do produto: "+this.getDescricao()+
                "\nValor do produto R$ "+this.getValor()+
                "\nMarca: "+this.getNome()+
                "\nCor: "+this.getCor()+
                "\nTamanho: "+this.getTamanho());
    } 

    @Override
    public void cadastraProduto() {
        super.cadastraProduto();
        setNome(JOptionPane.showInputDialog("Informe a marca: "));
        setCor(JOptionPane.showInputDialog("Informe a cor: "));
        setTamanho(JOptionPane.showInputDialog("Informe o tamanho: "));
    }
}
