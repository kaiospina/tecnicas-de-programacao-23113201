
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class Produto {
    private int codigo;
    private String descricao;
    private float valor;
    Produto(){}
    Produto( int c, String d, float v ){
        codigo = c;
        descricao = d;
        valor = v;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
    public void print(){
        JOptionPane.showMessageDialog(null,
                "Código do produto: "+codigo+"\nDescrição do produto: "+descricao+
                        "\nValor do produto R$ "+valor);
    }    
    
    public void cadastraProduto(){
        setCodigo(Integer.parseInt(JOptionPane.showInputDialog("Informe o código do produto: ")));
        setDescricao(JOptionPane.showInputDialog("Informe a tipo do produto: "));
        setValor(Float.parseFloat(JOptionPane.showInputDialog("Informe o preço:")));        
    }
    
}
