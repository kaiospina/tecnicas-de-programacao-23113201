
package exercicio_01_poo;

import javax.swing.JOptionPane;

public class Roupa extends Vestuario {
    private String tecido;
    Roupa(){}
    Roupa(int c, String d, float v, String n, String cr, String tm, String te){
        super(c, d, v, n, cr, tm);
        this.tecido = tm;
    }
        
    public String getTecido() {
        return tecido;
    }

    public void setTecido(String tecido) {
        this.tecido = tecido;
    }    
   
    public void print(){
                JOptionPane.showMessageDialog(null,
                        "Código do produto: "+this.getCodigo()+
                                "\nDescrição do produto: "+this.getDescricao()+
                                "\nValor do produto R$ "+this.getValor()+
                                "\nMarca: "+this.getNome()+
                                "\nCor: "+this.getCor()+
                                "\nTamanho: "+this.getTamanho()+
                                "\nTipo do tecido: "+this.getTecido());
}
     
    @Override
    
    public void cadastraProduto() {
        super.cadastraProduto();
        setTecido(JOptionPane.showInputDialog("Informe o tipo de tecido: "));
    }
}
