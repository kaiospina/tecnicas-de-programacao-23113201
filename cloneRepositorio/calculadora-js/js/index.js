var bloco1 = undefined;
var operacao = "";
var bloco2 = undefined;

/* Funcao chamada por qualquer numero*/
function escreveNum(num) {
    document.querySelector('#display').innerHTML += num;
}

/* Botao de reset */
var ac = document.querySelector('#clear');
ac.addEventListener('click', () => {
    document.querySelector('#display').innerHTML = "";
    bloco1 = undefined;
    operacao = "";
    bloco2 = undefined;
})

/* Funcao chamada quando operacao é decidida */
function decideOp(op) {
    bloco1 = document.querySelector('#display').innerHTML;
    document.querySelector('#display').innerHTML = "";
    operacao = op;
}

var botaoIgual = document.querySelector('#equals');
botaoIgual.addEventListener('click', () => {
    if(operacao != "" && document.querySelector('#display').innerHTML != ""){
        bloco2 = document.querySelector('#display').innerHTML;
        switch (operacao) {
            case "soma":
                document.querySelector('#display').innerHTML = parseFloat(bloco1) + parseFloat(bloco2);
                break;
            case "multiplicacao":
                document.querySelector('#display').innerHTML = bloco1 * bloco2;
                break;
            case "divisao":
                document.querySelector('#display').innerHTML = bloco1 / bloco2;
                break;
            case "subtracao":
                document.querySelector('#display').innerHTML = bloco1 - bloco2;
                break;
        }
        bloco1 = undefined;
        operacao = "";
        bloco2 = undefined;
    } 
})

/* Função chamada quando . é apertado */
var botaoPonto = document.querySelector('#dec');
botaoPonto.addEventListener('click', function(){
    document.querySelector('#display').innerHTML += ".";
})
